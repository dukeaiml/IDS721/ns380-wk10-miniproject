# ns380-wk10-miniproject

Creating a lambda function that utilizes the LLM crate and the pythia-160m model to serve question answering through a lambda.

## Build Process

The build process for this project is requires some assets. First you need to download a model from huggingface, and keep it in a known location in the repository. You can then build this binary using `cargo build`. You then need to copy it alongside the built application binary into a docker container to deploy.

To simplify this, the binary has been packaged with a Dockerfile. This Dockerfile will build the binary, including the predownloaded assets. Once the binary is built, it is pushed to ECR using

```
docker build -t <ECR Repo URL>:latest .
aws ecr get-login-password --region <region> | docker login --username AWS --password-stdin <ECR Repo URL>
docker push <ECR Repo URL>:latest
```

The lambda was then set up using the image from ECR as the base. It is not possible to use API Gateway to serve this Lambda because the amount of time the Lambda takes is too long and API Gateway has an unchangeable 30s timeout. I picked one of the smallest models (pythia-160m), set the Lambda to have 3GB RAM (which increases the compute power from the default), and it still barely completes within 2 minutes. Therefore, the function needs to be set up with a function URL from the lambda config page.

Here is the model card of the model I used https://huggingface.co/rustformers/pythia-ggml/blob/main/pythia-160m-q4_0-ggjt.bin. You can see that the file is only 100 MB, which should aid as much as possible with performance.

## Usage

This lambda is intended to be used with cURL. A command for usage should look like

```
curl -H "Content-Type: application/json" --data '{"input": "tigers are cool because"}' https://krnb555w3u6gbwmgszytiijjx40kdosc.lambda-url.us-west-2.on.aws
```

and should return an answer from the pythia-160m model.

![alt text](image.png)